import React from 'react'
import './App.css';
import ContactList from './components/ContactList';
import ContactSearch from './components/ContactSearch';

function App() {
  return (
    <React.Fragment>
      <ContactSearch/>
      <ContactList/>
    </React.Fragment>
      
  );
}

export default App;
