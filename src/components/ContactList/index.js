import React, { useState, useEffect} from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function CheckboxListSecondary({search}) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([]);
  const [contacts, setContacts] = useState([])
  useEffect(() => {
    axios
      .get("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")
      .then(response => {
        setContacts(response.data.sort((a, b) => a.last_name.localeCompare(b.last_name)))
      });
  }, []);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    console.log(newChecked)
    setChecked(newChecked);
  };
  const filterContacts = (value) => {
    if(search === '') return true;
    return `${value.first_name} ${value.last_name}`.toLowerCase().indexOf(search.toLowerCase()) !== -1
  }
  return (
      <List dense className={classes.root}>
        {contacts.filter(filterContacts).map((value) => {
          const labelId = `checkbox-list-secondary-label-${value.id}`;
          return (
            <ListItem key={value.id} button onClick={handleToggle(value.id)}>
              <ListItemAvatar>
                <Avatar
                  alt={`Avatar n°${value.id}`}
                  src={value.avatar}
                />
              </ListItemAvatar>
              <ListItemText id={labelId} primary={`${value.first_name} ${value.last_name}`} />
              <ListItemSecondaryAction>
                <Checkbox
                  onClick={handleToggle(value.id)}
                  edge="end"
                  checked={checked.indexOf(value.id) !== -1}
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
      </List>
  );
}