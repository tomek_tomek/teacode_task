import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    boxShadow: 'none',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

export default function CustomizedInputBase({setSearch}) {
  const classes = useStyles();
  const [inputValue, setInputValue] = useState('');
    return (
    <Paper className={classes.root}>
      <IconButton onClick={() => setSearch(inputValue)} className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
      <InputBase
        className={classes.input}
        placeholder="Find contact"
        inputProps={{ 'aria-label': 'find contact' }}
        value={inputValue}
        onChange={(event) => setInputValue(event.target.value)}
      />
    </Paper>
  );
}