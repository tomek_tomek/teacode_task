import React, { useState } from 'react'
import ContactList from '../ContactList';
import ContactSearch from '../ContactSearch';
import { StyledContainter } from './styles';
function App() {
  const [search, setSearch] = useState('');
  return (
    <StyledContainter>
      <ContactSearch setSearch={setSearch}/>
      <ContactList search={search}/>
    </StyledContainter>

  );
}

export default App;
